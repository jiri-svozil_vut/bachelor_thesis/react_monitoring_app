import React from 'react'
import {CheckboxInt} from '../App'
import Range from '../tempMulti/range'
import {Space} from 'antd';
import {format, isBefore, isAfter, add} from 'date-fns'
import AngryBen from '../AngryBen'
import moment from 'moment';
import {DatePicker, Radio} from 'antd';
import Axios from 'axios'
import AddData from '../functions/AddData';
import {GlobalFirstLast, every_nth, SetTempDataRange, Filterer, FetchData} from '../functions/Functions'


const {RangePicker} = DatePicker;


const DataRange = () => {

    const context = React.useContext(CheckboxInt)
    const {dates, tempRangeData, setTempRangeData} = context
    const [localRange, setLocalRange] = React.useState([])
    const [returned, setReturned] = React.useState(false)
    const [spacing, setSpacing] = React.useState(1)
    const [pickedDate, setPickedDate] = React.useState(null)


    function after_capture(data_servers, type, dateStrings) {
        var tempGlob = AddData(type, tempRangeData, data_servers)
        var tempik = SetTempDataRange(tempGlob, dateStrings)
        if (spacing !== 1) {
            var ever_NTH = localRange ? every_nth(tempik, spacing) : tempik
            setLocalRange(ever_NTH)
        } else {
            setLocalRange(tempik)
        }
        setTempRangeData(tempGlob)
    }


    function onChange(global_dates, dateStrings) {
        setReturned(false)
        setPickedDate(dates)
        var postValues


        /*
        *
        *
        * if (tempRangeData.length === 0) {
            postValues = {type: 'range', from: dateStrings[0], to: dateStrings[1]}
            FetchData(postValues, 'first', dates, after_capture, ['first', global_dates])
        } else {
            var rangeDates = GlobalFirstLast(tempRangeData, 60)
            if (isBefore(global_dates[1]._d, rangeDates[0])) {
                postValues = {type: 'range', from: dateStrings[0], to: dateStrings[1]}
                FetchData(postValues, 'first', dates, after_capture, ['first', global_dates])
            } else {
                if (isBefore(global_dates[0]._d, rangeDates[0])) {
                    postValues = {type: 'range', from: dateStrings[0], to: rangeDates[3]}
                    FetchData(postValues, 'before', dates, after_capture, ['before', global_dates])
                }
            }
            if (isAfter(dates[0]._d, rangeDates[1])) {
                postValues = {type: 'range', from: dateStrings[0], to: dateStrings[1]}
                FetchData('first', postValues, tempRangeData, global_dates)
            } else {
                if (isAfter(global_dates[1]._d, rangeDates[1])) {

                    postValues = {
                        type: 'range',
                        from: format(add(rangeDates[1], {seconds: 1}), "yyyy-MM-dd kk:mm:ss"),
                        to: dateStrings[1]
                    }
                    FetchData(postValues, 'after', dates, after_capture, ['after', global_dates])
                }
            }
        }
        *
        * if (postValues === undefined) {
            setReturned(false)
            var tempik = SetTempDataRange(tempRangeData, global_dates)
            setLocalRange(tempik)
        }
        *
        *
        * */

        postValues = {type: 'range', from: dateStrings[0], to: dateStrings[1]}
        FetchData(postValues, 'first', dates, after_capture, ['first', global_dates])

    }


    function setSpace(spacing) {
        setSpacing(spacing)
        setReturned(false)
        var tempik = SetTempDataRange(tempRangeData, pickedDate)
        if (spacing !== 1) {
            setLocalRange(every_nth(tempik, spacing))
        } else {
            setLocalRange(tempik)
        }
    }

    React.useEffect(() => {
        setReturned(true)
    }, [spacing])

    React.useEffect(() => {
        setReturned(true)
    }, [localRange])


    function onChangeSpacing(e) {
        setSpace(e.target.value)
    }

    const optionsSpacing = [
        {label: '1 Secs', value: 1},
        {label: '5 Secs', value: 5},
        {label: '2 Mins', value: 120},
    ]


    return (
        <div>
            <Space direction="vertical">
                <RangePicker
                    ranges={{'This minute': [moment().startOf('minute'), moment()]}}
                    showTime format="YYYY-MM-DD HH:mm:ss" onChange={onChange}
                    size='large'
                />
            </Space>
            <div className="current-spacing">
                <div className="current-spacing-name">Spacing:</div>
                <Radio.Group
                    options={optionsSpacing}
                    onChange={onChangeSpacing}
                    value={spacing}
                    optionType="button"
                    buttonStyle="solid"
                    size="large"
                />
            </div>
            <div className="informative">(for zoom and drag press "CTRL" key) {" "}</div>
            {localRange.length > 0 ? (returned ? <Range rangeData={localRange}/> : <AngryBen/>) : <AngryBen/>}
        </div>
    )
}

export default DataRange