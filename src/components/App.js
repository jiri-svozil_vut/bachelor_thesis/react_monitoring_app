import React, {createContext} from "react"
import "antd/dist/antd.css";
import "../index.css";
import Template from "./Template.js"
import {format} from 'date-fns'
import ListOfServers from "./ListOfServers.json"
import {FetchData} from "./functions/Functions"

const CheckboxInt = createContext()
ListOfServers.forEach((server) => {
    server['status'] = "WARNING"
})

const Main = ({children}) => {

    const [globalData, setGlobalData] = React.useState([])

    const mutationRef = React.useRef(globalData)


    var time = new Date()
    var newTime = new Date(time.getTime() - 60 * 1000)
    var tempObj = {
        from: format(newTime, 'yyyy-MM-dd kk:mm:ss'),
        to: format(time, 'yyyy-MM-dd kk:mm:ss')
    }


    const [timeInterval, setTimeInterval] = React.useState(tempObj) // casovy usek ktery se posle pro stazeni
    const [dates, setDates] = React.useState(ListOfServers)  // seznam vybranych serveru
    const valuesList = ['cpu/ram', 'bit_rate_in/bit_rate_out', 'packet_rate_in/packet_rate_out', 'tcp_established/tcp_syn_recv/tcp_syn_sent/tcp_fin_wait/tcp_time_wait/tcp_close_wait/tcp_closing','http_req', 'http_avg_req_size/http_avg_res_size',  'http_avg_res_time', "request_time"]
    const [valuesPost, setValuesPost] = React.useState('range')
    const [startStop, setStartStop] = React.useState(false)
    const [clickedServers, setClickedServers] = React.useState([])
    const [tempCurrentData, setTempCurrentData] = React.useState([])
    const [tempRangeData, setTempRangeData] = React.useState([])
    const [responseData, setResponseData] = React.useState({})

    return (<CheckboxInt.Provider value={{
        startStop, setStartStop,
        valuesPost, setValuesPost,
        clickedServers, setClickedServers,
        dates, setDates,
        valuesList,
        timeInterval, setTimeInterval,
        globalData, setGlobalData,
        responseData, setResponseData,
        mutationRef,
        tempCurrentData, setTempCurrentData,
        tempRangeData, setTempRangeData
    }}>{children}
    </CheckboxInt.Provider>)
}


function Second({children}) {
    const context = React.useContext(CheckboxInt)
    const {
        dates, startStop, timeInterval,
        globalData, setGlobalData
    } = context
    const [seconds, setSeconds] = React.useState(0)


    React.useEffect(() => {
        FetchData({type: "range", from: timeInterval.from, to: timeInterval.to}, 'first', dates, setGlobalData, [])
    }, [])


    React.useEffect(() => {
        const interval = setInterval(() => {
            setSeconds(seconds => seconds + 1)
        }, 1000)
        return () => clearInterval(interval)
    }, [])


    React.useEffect(() => {
        if (startStop) {
            serverStatus()
        }
    }, [seconds])


    function serverStatus() {
        var tempIp = dates.map((data) => {
            return data.ip
        })
        var golb
        dates.forEach((data) => {
            golb = globalData.map((globData, i) => {
                var stat = 0
                var ipaddr = Object.keys(globData)[0]
                if (tempIp.includes(ipaddr)) {
                    for (var k = 1; k < 6; k++) {
                        if (globData[ipaddr].cpu.at(0 - k) == null) {
                            stat = stat + 1
                        } else {
                            if (stat >= 2 && stat < 5) {
                                return {[ipaddr]: 2}
                            }
                            stat = 0
                            return {[ipaddr]: stat}
                        }
                    }
                    if (stat >= 5) {
                        return {[ipaddr]: stat}
                    }
                    if (stat >= 2) {
                        return {[ipaddr]: stat}
                    }
                }
            })
        })
        var status = Array(tempIp.length).fill(0)
        golb.forEach((datas, i) => {
            var ip = Object.keys(datas)[0]
            if (tempIp.includes(ip)) {
                status[tempIp.indexOf(ip)] = datas[ip]
                if (status[tempIp.indexOf(ip)] === 0) {
                    dates[tempIp.indexOf(ip)].status = 'OK'
                }
                if (status[tempIp.indexOf(ip)] === 2) {
                    dates[tempIp.indexOf(ip)].status = 'WARNING'
                }
                if (status[tempIp.indexOf(ip)] === 5) {
                    dates[tempIp.indexOf(ip)].status = 'CRITICAL'
                }
            }
        })
    }


    return (
        <div>
            {children}
        </div>
    )
}


const App = () => {
    return (
        <div>
            <Main>
                <Second>
                    <Template/>
                </Second>
            </Main>
        </div>
    )
}

export {CheckboxInt}
export default App
