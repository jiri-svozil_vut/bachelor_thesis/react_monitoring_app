export const AddData = (type, globalData, tempOBJ) => {


//postvalues je typ a casovy usek
//type je rozliseni
//globalData jsou globalData

    let tempServer
    var tempGlobal = [...globalData]
    var globalIps = []
    var fetchedIps = []
    tempGlobal.forEach((data) => {
        var globalKey = Object.keys(data)[0];
        globalIps.push(globalKey)
    })
    tempOBJ.forEach((data) => {
        var fetchedKey = Object.keys(data)[0];
        fetchedIps.push(fetchedKey)
    })

    tempOBJ.forEach((datas, i) => {  //fetched data
        var OBJIp = Object.keys(datas)[0] // ipadresa objektu
        if (globalIps.includes(OBJIp)) {
            if (tempGlobal[i][OBJIp] === undefined){
                var globalLength = 0
            } else {
                var globalLength = tempGlobal[i][OBJIp].timestamp.length
            }
            var globalIndex = globalIps.indexOf(OBJIp)
            tempServer = {...tempGlobal[globalIndex]}

            //replace data from server to avoid duplacates, and to replace the nulls
            if (type === 'before') {
                var arrayLength = datas[OBJIp].timestamp.length  //delka ziskanych dat

                const iterate_blacklist = ['id', 'name']
                for (const key in tempServer[OBJIp]) {
                    if (!iterate_blacklist.includes(key)) {
                        tempServer[OBJIp][key] = [...datas[OBJIp][key], ...tempServer[OBJIp][key]]
                    }
                }
                tempGlobal[globalIndex] = tempServer
            } else {
                arrayLength = datas[OBJIp].timestamp.length  //delka ziskanych dat

                const iterate_blacklist = ['id', 'name']
                for (const key in tempServer[OBJIp]) {
                    if (!iterate_blacklist.includes(key)) {
                        tempServer[OBJIp][key] = [...tempServer[OBJIp][key], ...datas[OBJIp][key]]
                    }
                }
            }
        }
        if (globalLength > 10000) {
            var diference = globalLength - 10000
            const iterate_blacklist = ['id', 'name']
            for (const key in tempServer[OBJIp]) {
                if (!iterate_blacklist.includes(key)) {
                    tempServer[OBJIp][key] = tempServer[OBJIp][key].slice(diference)
                }
            }
        }

        if (!globalIps.includes(OBJIp)) {  //pokud v global neni tento server
            if (tempGlobal.length >= 1 && !(tempGlobal[0][Object.keys(tempGlobal[0])].timestamp.at(0) === datas[OBJIp].timestamp.at(0))) { //pokud uz tam neco je, ale pridam na zacatek null, aby vse bylo stejne dlouhe.
                tempServer = {...datas}
                globalLength = tempGlobal[0][Object.keys(tempGlobal[0])].timestamp.length

                const tempArrPre = Array(globalLength - 1).fill(null)
                const iterate_blacklist = ['id', 'timestamp', 'name']
                for (const key in tempServer[OBJIp]) {
                    if (!iterate_blacklist.includes(key)) {
                        tempServer[OBJIp][key] = [...tempArrPre, ...tempServer[OBJIp][key]]
                    }
                }
                tempServer[OBJIp].timestamp = [...tempGlobal[0][Object.keys(tempGlobal[0])].timestamp, ...tempServer[OBJIp].timestamp,]
                tempGlobal[tempGlobal.length] = tempServer
            } else {
                tempGlobal[tempGlobal.length] = {...datas}
            }
        }
        tempGlobal.forEach((datas2, i2) => {
            var tempIPglob = Object.keys(datas2)[0]
            if (!fetchedIps.includes(tempIPglob)) {
                tempServer = {...datas2}
                const tempArrAft = Array(arrayLength).fill(null)

                const iterate_blacklist = ['id', 'timestamp']
                for (const key in tempServer[tempIPglob]) {
                    if (!iterate_blacklist.includes(key)) {
                        tempServer[tempIPglob][key] = [...tempServer[tempIPglob][key], ...tempArrAft]
                    }
                }
                tempServer[tempIPglob].timestamp = [...tempServer[tempIPglob].timestamp, ...datas[OBJIp].timestamp]
            }
        })
    })

    return tempGlobal
}

export default AddData
