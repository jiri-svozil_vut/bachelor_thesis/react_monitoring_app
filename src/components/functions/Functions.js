import React from 'react'
import {sub, format, isBefore} from 'date-fns'
import Axios from "axios";
import AddData from "./AddData";
import axios from "axios";


const GlobalFirstLast = (data, secsToSub) => {

    var getGlobalFirst, getGlobalLast
    data.forEach((data, i) => {
        var ipadr = Object.keys(data)[0]
        getGlobalFirst = data[ipadr].timestamp.at(0).split(".")[0].replace("T", " ") // prvni global cas
        getGlobalLast = data[ipadr].timestamp.at(-1).split(".")[0].replace("T", " ")
    })
    var first = DateFormater(getGlobalFirst)
    var last = DateFormater(getGlobalLast)
    var globalFirst = new Date(first.year, first.month, first.day, first.hour, first.minute, first.second)
    var globalLast = new Date(last.year, last.month, last.day, last.hour, last.minute, last.second)
    var globalminusSec = sub(globalFirst, {seconds: 1})
    let interval = sub(globalLast, {seconds: secsToSub})

    return [globalFirst, globalLast, interval, globalminusSec]

}


const DateFormater = function (date) { // format yyyy-mm-dd HH:MM:SS
    const datum = {
        year: Number(date.split('-')[0]),
        month: Number(date.split('-')[1]) - 1,
        day: Number(date.split('-')[2].split(' ')[0]),
        hour: Number(date.split(':')[0].split(' ')[1]),
        minute: Number(date.split(' ')[1].split(':')[1]),
        second: Number(date.split(' ')[1].split(':')[2])
    }
    return datum
}

const SetPostValues = (type, globalDates) => {
    var postValues

    if (type === 'before') { // = range, curent
        var tempInterval = {
            type: 'range',
            from: format(globalDates[2], "yyyy-MM-dd kk:mm:ss"),
            to: format(globalDates[3], "yyyy-MM-dd kk:mm:ss")
        }
        if (isBefore(globalDates[2], globalDates[0])) {
            postValues = tempInterval
        } else {
            return null
        }
    }
    if (type === 'update') {
        postValues = {type: "update", last: format(globalDates[1], "yyyy-MM-dd kk:mm:ss")}
    }
    return postValues
}


const StatusSign = (props) => {

    if (props.stat === 'OK')
        return <p className="statusDotOk"></p>
    if (props.stat === 'WARNING') {
        return <div className="statusDotWarning"></div>
    }
    if (props.stat === 'CRITICAL') {
        return <div className="statusDotCritical"></div>
    }
}


const SetTempData = (data, dates, secsToSub) => {
    var tempData = data.map((server, i) => {
        var ipaddr = Object.keys(server)[0]
        var fromTime = format(dates[2], "yyyy-MM-dd'T'kk:mm:ss.'000000+0200'")
        var ind
        if (server[ipaddr].timestamp.includes(fromTime)) {
            ind = server[ipaddr].timestamp.indexOf(fromTime)
        } else {
            ind = 0 - secsToSub
        }
        let output = {
            [ipaddr]: {
                name: server[ipaddr].name,
                description: server[ipaddr].description,
            }
        }
        let iterate_blacklist = ['id', 'name', 'description']
        for (const key in server[ipaddr]) {
            if (!iterate_blacklist.includes(key)) {
                output[ipaddr][key] = server[ipaddr][key].slice(ind)
            }
        }
        return output
    })
    return tempData
}


const every_nth = (data, nth) => {
    var tempFilter = [...data]
    var tempFilterServer
    tempFilter.forEach((date, i) => {
        var ipadddr = Object.keys(date)[0]
        tempFilterServer = {...date}
        const iterate_blacklist = ['id', 'name']
        for (const key in tempFilterServer[ipadddr]) {
            if (!iterate_blacklist.includes(key)) {
                tempFilterServer[ipadddr][key] = tempFilterServer[ipadddr][key].filter((e, i) => i % nth === nth - 1)
            }
        }
        tempFilter[i] = tempFilterServer
    })
    return tempFilter // cely data, at se neseru a state
}
const SetTempDataRange = (tempGlob, dateStrings) => {
    var getIndexFirst, getIndexLast
    var tempData = tempGlob.map((server, i) => {
        var ipaddr = Object.keys(server)[0]
        var fromTime = format(dateStrings[0]._d, "yyyy-MM-dd'T'kk:mm:ss.'000000+0200'")
        var toTime = format(dateStrings[1]._d, "yyyy-MM-dd'T'kk:mm:ss.'000000+0200'")
        getIndexFirst = server[ipaddr].timestamp.indexOf(fromTime)
        getIndexLast = server[ipaddr].timestamp.indexOf(toTime) + 1

        let output = {
            [ipaddr]: {
                name: server[ipaddr].name,
                description: server[ipaddr].description,
            }
        }
        let iterate_blacklist = ['id', 'name', 'description']
        for (const key in server[ipaddr]) {
            if (!iterate_blacklist.includes(key)) {
                output[ipaddr][key] = server[ipaddr][key].slice(getIndexLast, getIndexFirst)
            }
        }

        return output
    })
    return tempData
}

function Filterer(Servers, data) {
    var Ips = []
    Servers.forEach((data) => {
        var globalKey = data.ip;
        Ips.push(globalKey)
    })
    var result = data.filter(servers => Ips.includes(servers.info.ip))
    return result
}

function GetFromServer(postValues, server_ip, dates) {
    let tempOBJ = []
    return Axios.post(server_ip, postValues, {
        timeout: 2000,
        headers: {'Content-Type': 'application/json'}
    }).then(function (response) {
        if (!response.data.error) {
            let newData = Filterer(dates, response.data.data)
            newData.forEach((datas) => {
                const iterate_blacklist = ['id', 'name', 'description']
                let newServer = {
                    [datas.info.ip]: {
                        name: datas.info.name,
                        description: datas.info.os,
                    }
                }

                const keys = Object.keys(datas.values[0])
                keys.forEach((key) => {
                    if (!iterate_blacklist.includes(key)) {
                        newServer[[datas.info.ip]][key] = datas.values.map((data) => {
                            return data[key]
                        })
                    }
                })
                tempOBJ.push(newServer)
            })
            return tempOBJ[0]
        }
    }).catch((error) => console.log(`IP: ${server_ip} ${error}`))
}


function FetchData(post_values, type, dates, do_func, do_args) {
    let data_servers = []
    let promises = []

    dates.forEach((server) => {
        const ip = `http://${server.ip}:${server.port}`
        promises.push(
            GetFromServer(post_values, ip, dates)
                .then((data) => {
                    if (data) {
                        data_servers.push(data)
                    }
                })
        )
    })

    Promise.all(promises).then(() => {
        do_func(data_servers, ...do_args)
    })
}

function response_time(server) {

    const url = `http://${server.ip}:80`
    const startTime = new Date().toISOString();
    const timestamp = Date.now();
    return axios({url: url, method: "get", timeout: 1000, params: {_cache: timestamp}}).then((response) => {
        if(response.status === 200){
            const endTime = new Date().toISOString();
            const duration = Date.parse(endTime) - Date.parse(startTime);

            const FormatStartTime = new Date(startTime).toISOString().slice(0, -4) + "000000+0000";

            return {
                response_timestamp: FormatStartTime,
                response_values: duration
            }
        }



    }).catch(error => {
        console.error('Error:', error);
        return {
            response_timestamp: startTime,
            response_values: null
        }
    })
}


function get_responses(dates, do_func) {
    let data_servers = {}
    let promises = []
    dates.forEach((server) => {
        promises.push(response_time(server).then((data) => {
            if (data) {
                data_servers[server.ip] = data
            }
        }))
    })

    Promise.all(promises).then(()=>{
        do_func(data_servers)
    })
}



export {
    DateFormater,
    GlobalFirstLast,
    SetPostValues,
    StatusSign,
    SetTempData,
    every_nth,
    SetTempDataRange,
    Filterer,
    GetFromServer,
    FetchData,
    get_responses
}



