import React from 'react';
import GraphCurrent from "../graphs/GraphCurrent"
import {Chart, registerables} from 'chart.js'
import AngryJOe from '../AngryJOe'
import {CheckboxInt} from '../App'
import GraphResponse from "../graphs/GraphResponse";
Chart.register(...registerables)


const Current = (props) => {

    const context = React.useContext(CheckboxInt)
    const {clickedServers} = context

    return (
        <div>
            {

                clickedServers.length > 0 ? clickedServers.map((temp) =>
                    props.tempData ? props.tempData.map((temp2) => {
                        var ipaddr = Object.keys(temp2)
                        let graph_type= temp.split(" ")[1]
                        if (graph_type === "request_time"){
                            if (ipaddr[0] === temp.split(" ")[0]) {
                                return <GraphResponse key={ipaddr + temp.split(" ")[1]} dataValue={props.responseData[ipaddr]} ipAddr={ipaddr} name={temp2[ipaddr].name} />;

                            } else {
                                return null
                            }

                        }else {
                            if (ipaddr[0] === temp.split(" ")[0]) {
                                return <GraphCurrent key={ipaddr + temp.split(" ")[1]}
                                                     cpuram={false} dataInfo={temp} dataValue={temp2} ipAddr={ipaddr}
                                                     name={temp2[ipaddr].name} graph_type={graph_type}
                                />
                            } else {
                                return null
                            }
                        }

                    }) : <AngryJOe/>) : <AngryJOe/>
            }
        </div>
    )
}

export default Current;