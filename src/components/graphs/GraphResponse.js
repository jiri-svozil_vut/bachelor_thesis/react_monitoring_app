import {Button} from "antd";
import {DownloadOutlined} from "@ant-design/icons";
import {Bar} from "react-chartjs-2";
import React from "react";

const GraphResponse = (props) => {
    let timestamps;
    let values;
    if (props.dataValue === undefined){
        timestamps = []
        values = []
    }else {
        timestamps = props.dataValue.response_timestamp.map((datas) => datas.split(".")[0].split("T")[1])
        values = props.dataValue.response_values

    }

    var dataJson = {
        labels: timestamps,
        datasets: [
            {
                label: 'Response time',
                data: values,
                backgroundColor: 'rgba(0, 140, 255, 0.5)',
            }
        ]
    }


    const downloadImg = React.useRef(null);
    const downloadImage = React.useCallback(() => {
        var a = document.createElement('a');
        a.download = 'chart';
        a.href = downloadImg.current.toBase64Image();
        a.click();
    }, []);



    const optionsJson = {
        elements: {
            point: {
                radius: 1
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        animation: {
            duration: 0
        },
        plugins: {
            zoom: {
                zoom: {
                    wheel: {
                        enabled: true,
                        modifierKey: 'ctrl'
                    },
                    mode: "x",
                    speed: 100,
                    drag: {
                        enabled: true,
                        backgroundColor: 'lightblue',
                        borderColor: 'blue',
                        borderWidth: 1,

                    }
                },
                pan: {
                    enabled: true,
                    mode: "x",
                    speed: 100,
                    modifierKey: 'ctrl'
                }
            },
        },
        scales: {
            "y": {
                "display": true,
                "position": "left"
            },
            "y1": {
                "display": false,
                "position": "right"
            }
        }
    };

    return (
        <div>
            <div className='whole-graph'>
                <div className='graph-title'>{`${props.ipAddr} - ${props.name}`}</div>
                <div className='graph-title-name'>Response time</div>
                <Button icon={<DownloadOutlined/>} onClick={downloadImage} className="btn-download">Download</Button>
                <Bar height={500}
                     width={2000}
                     options={optionsJson}
                     data={dataJson}
                     className="templateGraf"
                />
            </div>
        </div>
    )
}

export default GraphResponse