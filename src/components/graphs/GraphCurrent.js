import React from 'react'
import {Line} from 'react-chartjs-2'
import {Chart, registerables} from 'chart.js'
import {Button} from 'antd';
import {CheckboxInt} from '../App'
import {DownloadOutlined} from '@ant-design/icons';
import GraphOptions from './graphOptions.json'


Chart.register(...registerables)
var errors = []
const GraphCurrent = (props) => {

    const context = React.useContext(CheckboxInt)
    const {valuesList} = context

    var firstTimeStamp = props.dataValue[props.ipAddr].timestamp.at(0)

    const graphs_names = {
        'cpu/ram' : 'CPU/RAM',
        'bit_rate_in/bit_rate_out': 'Bit rate',
        'packet_rate_in/packet_rate_out': 'Packet rate',
        'tcp_established/tcp_syn_recv/tcp_syn_sent/tcp_fin_wait/tcp_time_wait/tcp_close_wait/tcp_closing': 'TCP Statements',
        'http_req' : 'HTTP Requests',
        'http_avg_req_size/http_avg_res_size': 'HTTP Packet size',
        'http_avg_res_time' : 'HTTP Time'
    }


    React.useEffect(() => {
        var lenghtOfData = props.dataValue[props.ipAddr].timestamp.length
        errors = []
        for (var i = 0; i < lenghtOfData; i++) {
            if (props.dataValue[props.ipAddr].cpu[i] == null) {
                errors = [...errors, 100]
            } else {
                errors = [...errors, 0]
            }
        }

    }, [firstTimeStamp])

    var indexOfValues = valuesList.indexOf(props.dataInfo.split(" ")[1]) + 1
    var dataJson = {
        labels: props.dataValue[props.ipAddr].timestamp.map((datas) => datas.split(".")[0].split("T")[1]),
        datasets: []
    }


    let values = props.graph_type.split('/')
    values.forEach((value, i) => {
        let graph_option = GraphOptions.filter((el) => el.id === value)[0]
        dataJson.datasets.push({
                title: props.ipAddr,
                label: value,
                data: props.dataValue[props.ipAddr][value],
                fill: true,
                backgroundColor: graph_option.data.backgroundColor,
                borderColor: graph_option.data.borderColor
            })
    })
    dataJson.datasets.push({
        title: 'error',
        label: 'error',
        data: errors,
        fill: true,
        backgroundColor: 'rgba(255, 0, 0, .2)',
        borderColor: 'rgba(255, 0, 0, 1)',
        yAxisID: 'y'
    })

    const optionsJson = {
        elements: {
            point: {
                radius: 1
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        animation: {
            duration: 0
        },
        plugins: {
            zoom: {
                zoom: {
                    wheel: {
                        enabled: true,
                        modifierKey: 'ctrl'
                    },
                    mode: "x",
                    speed: 100,
                    drag: {
                        enabled: true,
                        backgroundColor: 'lightblue',
                        borderColor: 'blue',
                        borderWidth: 1,

                    }
                },
                pan: {
                    enabled: true,
                    mode: "x",
                    speed: 100,
                    modifierKey: 'ctrl'
                }
            },
        },
        scales: GraphOptions[indexOfValues].options.scales
    }


    const downloadImg = React.useRef(null);
    const downloadImage = React.useCallback(() => {
        var a = document.createElement('a');
        a.download = 'chart';
        a.href = downloadImg.current.toBase64Image();
        a.click();
    }, []);
    return (
        <div>
            <div className='whole-graph'>
                <div className='graph-title'>{`${props.ipAddr} - ${props.name}`}</div>
                <div className='graph-title-name'>{graphs_names[props.graph_type]}</div>
                <Button icon={<DownloadOutlined/>} onClick={downloadImage} className="btn-download">Download</Button>
                <Line
                    data={dataJson}
                    height={500}
                    width={2000}
                    options={optionsJson}
                    ref={downloadImg}
                    className="templateGraf"
                />
            </div>
        </div>
    )
}
export default GraphCurrent
